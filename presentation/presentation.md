---
title:
- Current audio research<br> @SAT Metalab
author:
- Nicolas Bouillot
institute:
- Société des arts technologiques
theme:
- moon
date:
- 25 May 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

##  

* Société des arts technologiques [SAT]
  <p> Artist centre in Montreal
* Metalab
 <p> applied R&D at [SAT]

* Contributions from
<p> Émile Ouellet-Delorme, Emmanuel Durand, Gabriel Downs, Harish Venkatesan, Hector Teyssier, Jean-Yves Münch, Marc Lavallée, Michal Seta, Nicolas Bouillot, Raphaël Duée, Thomas Piquet, Zack Settel 


## Immersive 6 Degrees of Freedom (6 DoF) navigation - near and far fields 

<iframe src="https://player.vimeo.com/video/522509890?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/522509890">Immersive navigation in our dome, with SATIE, our spatial audio engine</a></p>

## 6 DoF with measured room impulse responses (RIR)

<iframe src="https://player.vimeo.com/video/697114857?h=db6207dbaf" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/697114857">6 DoF navigation generated from RIR</a></p>

## Live acoustic simulation

<iframe src="https://player.vimeo.com/video/484122810?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/484122810">Live acoustic simulation with vaRays</a></a></p>

## Links

* SATIE - audio spatialization  
  <p><a href="https://sat-metalab.gitlab.io/satie/">https://sat-metalab.gitlab.io/satie/</a></p>
* vaRays - live 3D acoustic simulation
  <p><a href="https://sat-metalab.gitlab.io/satie/">https://sat-metalab.gitlab.io/satie/</a></p>
* Audiodice - Near field immersive speakers
  <p><a href="https://gitlab.com/sat-metalab/hardware/audiodice">https://gitlab.com/sat-metalab/hardware/audiodice</a></p>
* 6 DoF public datasets (audio and RIR)
  <p><a href="https://archive.org/details/savr_audio_dataset">https://archive.org/details/savr_audio_dataset</a></p>
  <p><a href="https://archive.org/details/savr_rir_dataset_202204">https://archive.org/details/savr_rir_dataset_202204</a></p>

nbouillot@sat.qc.ca

